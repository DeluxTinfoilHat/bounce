
try:
    import tkinter as tk
    from tkinter import *
    from tkinter.font import Font
    
except ImportError:
    import Tkinter as tk
    from Tkinter import *
    from Tkinter.font import Font

import time

bouncieness_factor = -1
WIDTH = 50
HEIGHT = 600
SIZE = 50




class Ball:
    def __init__(self, parent, puck, pos = 0, speed = 8, mass = 1):
        self.parent = parent

        self.mass = mass
        if puck == True:
            self.shape = self.parent.canvas.create_oval(SIZE*0.25, pos, SIZE*0.75, SIZE*0.5+pos, fill="blue")
        else:
            self.shape = self.parent.canvas.create_rectangle(0, pos, SIZE, SIZE+pos, fill="red")
        self.velocity = speed # changed from 3 to 9
        self.velocityOld =speed

    def ball_update(self):
        self.parent.canvas.move(self.shape, 0.0, self.velocity)
        self.velocityOld = self.velocity

    def check_collision(self, m2, v2):

        m1 = self.mass
        v1 = self.velocity

        x1, y1, x2, y2 = self.parent.canvas.coords(self.shape)

        result = self.parent.canvas.find_overlapping(x1, y1, x2, y2)

        if len(result) > 1 :
            print(result)
            self.velocity = (((m1-m2)/(m1+m2))*v1 + ((2*m2)/(m1+m2))*v2)
            return

        pos = self.parent.canvas.coords(self.shape)

        if pos[3] >= HEIGHT or pos[1] <= 0:
            self.velocity *= -1

        return

    def get_coords(self):
        return self.parent.canvas.coords(self.shape)


class MainApplication(tk.Frame):

    def update(self):

        A = self.balls[0]
        B = self.balls[1]

        A.check_collision(B.mass, B.velocityOld)
        B.check_collision(A.mass, A.velocityOld)

        for ball in self.balls:
            ball.ball_update()

        self.after(30, self.update) # changed from 10ms to 30ms

    def reset_stuff(self):
        self.canvas.delete("all")
        self.balls = []
        m1 = 10.0
        m2 = 0.01
        v1 = 1.5
        v2 = 0.0
        temp = str(self.newinfo.get())
        if len(temp) > 1:
            m1, v1, m2, v2 = temp.split(",")
        self.balls.append(Ball(self, False, 10, float(v1), float(m1)))
        self.balls.append(Ball(self, True, 100, float(v2), float(m2)))

    ## application initialize (parent is root from __main__)
    def __init__(self, parent, *args, **kwargs):

        Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent

        self.canvas = Canvas(self, width=WIDTH, height=HEIGHT, bg="grey")
        self.canvas.pack(side = "bottom")
        color = 'black'

        self.reset = Button(self, width = 8, height = 2, bg = "yellow", text = "RESET", command = self.reset_stuff)
        self.reset.pack(side = "top")
        self.labl = Label(text = "m1, v1, m2, v2")
        self.labl.pack(side = "top")
        self.newinfo = tk.StringVar()
        self.l1 = Entry(textvariable = self.newinfo)
        self.l1.pack(side = "top")

        self.balls = []
        self.balls.append(Ball(self, False, 10, 1.2, 10))
        self.balls.append(Ball(self, True, 100, 0, 0.05))
        

        self.update()

##----------------------------------------------------------##


if __name__ == "__main__":
    root = tk.Tk()

    root.title("bouncing")
    MainApplication(root).pack(side="top", fill="both")
    root.mainloop()


##----------------------------------------------------------##
    
